Imports System.IO

Public Class GoALMA

    Private Const chromeEXE As String = "C:\Documents and Settings\paulx030a\Local Settings\Application Data\Google\Chrome\Application\chrome.exe"
    'Private Const chromeEXE As String = "C:\Program Files\Google\Chrome\Application\chrome.exe"

    Dim windowsNTaccount As String
    Dim machineName      As String
    Dim authenticateMD5  As String
    Dim myProcess        As New Process

    Private Sub GoALMA_Load( ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If File.Exists(chromeEXE) = False
            Me.Hide
            MsgBox( _
                  "ALMA 2 can't be started as it appears" _
                & vbCr _
                & "this machine does not have Google Chrome", _
                MsgBoxStyle.Critical Or _
                MsgBoxStyle.OkOnly Or _
                MsgBoxStyle.SystemModal, _
                "ALMA 2")
            End
        End If

        Me.Cursor = Cursors.AppStarting

        windowsNTaccount = LCase(Environment.UserName)
        machineName      = UCase(Environment.MachineName)
        authenticateMD5  = getMD5Hash(windowsNTaccount & Date.Today & "GoALMA")

        myProcess.StartInfo.FileName = chromeEXE
        myProcess.StartInfo.UseShellExecute = False
        myProcess.StartInfo.CreateNoWindow  = True
        myProcess.StartInfo.Arguments = "http://alma.io/" _
            & windowsNTaccount & "/" _
            & authenticateMD5  & "/" _
            & machineName & "/"

        myProcess.Start()

        tmrUnload.Enabled = True

        myProcess.Close()
        myProcess.Dispose()

    End Sub


    Private Sub tmrUnload_Tick( ByVal sender As System.Object,  ByVal e As System.EventArgs) Handles tmrUnload.Tick
        If Me.Opacity <= 0.0 Then
            tmrUnload.Enabled = False
            End
        Else
            Me.Opacity -= 0.05
        End If
    End Sub


    Public Function getMD5Hash(ByVal strToHash As String) As String
        Dim b             As Byte
        Dim strResult     As String = ""
        Dim md5Obj        As New System.Security.Cryptography.MD5CryptoServiceProvider()
        Dim bytesToHash() As Byte = System.Text.Encoding.ASCII.GetBytes(strToHash)

        bytesToHash = md5Obj.ComputeHash(bytesToHash)
        For Each b In bytesToHash
            strResult += b.ToString("x2")
        Next

        Return strResult
    End Function

End Class
